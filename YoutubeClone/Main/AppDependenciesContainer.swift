//
//  AppDependenciesContainer.swift
//  YoutubeClone
//
//  Created by Luka on 17/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

class AppDependenciesContainer: HomeViewControllerDataSourceDependencies, SearchViewControllerDataSourceDependencies{
    var imageUseCase: ImageUseCase = ImageInteractor(provider: GAPIImageProvider())
    var homeFeedUseCase: HomeVideosUseCase = HomeVideosInteractor(provider: GAPIHomeVideosProvider())
    var trendingFeedUseCase: TrendingVideosUseCase = TrendingVideosInteractor(provider: GAPITrendingVideosProvider())
    var subscriptionFeedUseCase: SubscriptionVideosUseCase = SubscriptionVideosInteractor(provider: GAPISubscriptionVideosProvider())
    var searchedVideosUseCase: SearchUseCase = SearchInteractor(provider: GAPISearchProvider())
    var channelUseCase: ChannelUseCase = ChannelInteractor(provider: GAPIChannelProvider())
}
