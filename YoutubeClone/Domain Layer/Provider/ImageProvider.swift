//
//  ImageProvider.swift
//  YoutubeClone
//
//  Created by Luka on 17/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

protocol ImageProvider{
    func fetchImage(for path: String, completion: @escaping (Response<Data>) -> Void)
}
