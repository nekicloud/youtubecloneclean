//
//  SubscriptionVideosProvider.swift
//  YoutubeClone
//
//  Created by Luka on 19/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

protocol SubscriptionVideosProvider{
    func fetchSubscriptionVideos(completion: @escaping(Response<Videos>) -> Void)
}
