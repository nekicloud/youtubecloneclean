//
//  Video.swift
//  YoutubeClone
//
//  Created by Luka on 15/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

protocol Video{
    var title: String {get}
    var numberOfViews: Int{get}
    var uploadDate: Date{get}
    var thumbnailImagePath: String {get}
    var channelName: String{get}
    var channelId: String{get}
    var channelImagePath: String{get set}
    var videoID: String {get set}
}

typealias Videos = [Video]
