//
//  Channel.swift
//  YoutubeClone
//
//  Created by Luka on 19/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

protocol Channel{
    var channelName: String {get}
    var channelId: String{get}
    var channelImagePath: String{get set}
}
