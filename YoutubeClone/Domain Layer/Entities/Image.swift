//
//  Channel.swift
//  YoutubeClone
//
//  Created by Luka on 16/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

struct Image{
    let path: String
    var image: Data
    
    init(path: String, image: Data) {
        self.path = path
        self.image = image
    }
}
