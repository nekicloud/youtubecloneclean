//
//  ImageUseCase.swift
//  YoutubeClone
//
//  Created by Luka on 17/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

protocol ImageUseCase{
    func fetchImage(path: String, completion: @escaping (Response<Image>) -> Void)
}
