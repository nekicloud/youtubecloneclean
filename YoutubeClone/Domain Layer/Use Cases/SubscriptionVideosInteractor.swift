//
//  SubscriptionVideosProvider.swift
//  YoutubeClone
//
//  Created by Luka on 19/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

class SubscriptionVideosInteractor: SubscriptionVideosUseCase{
    let provider: SubscriptionVideosProvider
    
    init(provider: SubscriptionVideosProvider) {
        self.provider = provider
    }
    
    func fetchSubscriptionVideos(completion: @escaping (Response<Videos>) -> Void) {
        provider.fetchSubscriptionVideos(completion: completion)
    }
}
