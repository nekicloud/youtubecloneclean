//
//  ImageInteractor.swift
//  YoutubeClone
//
//  Created by Luka on 17/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

class ImageInteractor: ImageUseCase{
    let provider: ImageProvider
    
    init(provider: ImageProvider){
        self.provider = provider
    }
    
    func fetchImage(path: String, completion: @escaping (Response<Image>) -> Void) {
        provider.fetchImage(for: path, completion:{ (response) in
            switch response{
            case .success(let imageData):
                completion(.success(Image(path: path, image: imageData)))
            case .error(let error):
                print(error.localizedDescription)
            }
        })
    }
}
