//
//  SearchProvider.swift
//  YoutubeClone
//
//  Created by Luka on 19/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

class SearchInteractor:SearchUseCase{
    let provider: SearchProvider
    
    init(provider: SearchProvider) {
        self.provider = provider
    }
    
    func fetchSearchedVideos(searchTerm: String, completion: @escaping (Response<Videos>) -> Void) {
        provider.fetchSearchedVideos(searchTerm: searchTerm, completion: completion)
    }
}
