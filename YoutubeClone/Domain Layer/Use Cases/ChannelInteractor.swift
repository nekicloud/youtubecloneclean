//
//  ChannelInteractor.swift
//  YoutubeClone
//
//  Created by Luka on 19/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

class ChannelInteractor: ChannelUseCase{
    let provider: ChannelProvider
    
    init(provider: ChannelProvider) {
        self.provider = provider
    }
    
    func fetchChannelImageURL(for Id: String, completion: @escaping (Response<String>) -> Void) {
        provider.fetchChannelImageURL(for: Id, completion: completion)
    }
}
