//
//  HomeVideosProvider.swift
//  YoutubeClone
//
//  Created by Luka on 19/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

class HomeVideosInteractor: HomeVideosUseCase{
    let provider: HomeVideosProvider
    
    init(provider: HomeVideosProvider) {
        self.provider = provider
    }
    
    func fetchHomeVideos(completion: @escaping (Response<Videos>) -> Void) {
        provider.fetchHomeVideos(completion: completion)
    }
}
