//
//  TrendingVideosProvider.swift
//  YoutubeClone
//
//  Created by Luka on 19/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

class TrendingVideosInteractor: TrendingVideosUseCase{
    let provider: TrendingVideosProvider
    
    init(provider: TrendingVideosProvider) {
        self.provider = provider
    }
    
    func fetchTrendingVideos(completion: @escaping (Response<Videos>) -> Void) {
        provider.fetchTrendingVideos(completion: completion)
    }
}
