//
//  ChannelUseCase.swift
//  YoutubeClone
//
//  Created by Luka on 19/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

protocol ChannelUseCase {
    func fetchChannelImageURL(for Id: String, completion: @escaping (Response<String>) -> Void)
}
