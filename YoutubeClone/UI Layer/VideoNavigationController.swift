//
//  NavigationController.swift
//  YoutubeClone
//
//  Created by Luka on 15/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit

class VideoNavigationController:UINavigationController {
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    func setUpNavigationController() {
        navigationBar.barTintColor = UIColor(red: 230/255, green: 32/255, blue: 31/255, alpha: 1)
        navigationBar.isTranslucent = false
        navigationBar.shadowImage = UIImage()
    }
}
