//
//  AppCoordinator.swift
//  YoutubeClone
//
//  Created by Luka on 15/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit

class AppCoordinator{
    let window: UIWindow
    let dependencies: AppDependenciesContainer
    let homeViewController: HomeViewController
    var navController: VideoNavigationController
    private var emptyView: UIView?
    private var settingsView: SettingsView?
    private var searchBox: SearchBoxView?
    
    init(window: UIWindow = UIWindow(frame: UIScreen.main.bounds)){
        self.window = window
        self.dependencies = AppDependenciesContainer()
        self.homeViewController = HomeViewController(dependencies: dependencies)
        self.navController = VideoNavigationController(rootViewController: homeViewController)
    }
    
    func start(){
        navController.setUpNavigationController()
        setUpDelegates()
        window.rootViewController = navController
        window.makeKeyAndVisible()
    }
    
    private func setUpDelegates(){
        homeViewController.searchDelegate = self
        homeViewController.videoDelegate = self
        homeViewController.settingsDelegate = self
    }
}

extension AppCoordinator: VideoViewControllerDelegate {
    func showVideoFor(url: URLRequest, title: String) {
        let videoViewController = VideoViewController(videoURL: url, title: title)
        navController.pushViewController(videoViewController, animated: true)
    }
}

extension AppCoordinator: SettingsDisplayDelegate{
    func showSettings() {
        emptyView = UIView()
        guard let blackView = emptyView else {return}
        blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(settingsDismissSelected)))
        blackView.backgroundColor = UIColor(white: 0, alpha: 0.5)
        blackView.frame = window.frame
        blackView.alpha = 0
        window.addSubview(blackView)
        settingsView = SettingsView(frame: CGRect(x: window.frame.origin.x, y: window.frame.maxY, width: window.frame.width, height: window.frame.height/2), dismissDelegate: self)
        guard let safeSettingsView = settingsView else{return}
        window.addSubview(safeSettingsView.view)
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            blackView.alpha = 1
            safeSettingsView.view.frame = CGRect(x: self.window.frame.origin.x, y: self.window.frame.maxY/2, width: self.window.frame.width, height: self.window.frame.height/2)
        }, completion: nil)
    }
    
    @objc private func settingsDismissSelected(){
        UIView.animate(withDuration: 0.5) {
            guard let blackView = self.emptyView else {return}
            blackView.alpha = 0
            guard let safeSettingsView = self.settingsView else{return}
            safeSettingsView.view.frame = CGRect(x: self.window.frame.origin.x, y: self.window.frame.maxY, width: self.window.frame.width, height: self.window.frame.height/2)
        }
    }
    
    func showControllerForSetting(setting: Setting) {
        let settingViewcontroller = UIViewController()
        settingViewcontroller.view.backgroundColor = .white
        navController.pushViewController(settingViewcontroller, animated: true)
    }
}

extension AppCoordinator: SettingsDismissDelegate {
    func handleDismiss(setting: Setting){
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            guard let blackView = self.emptyView else {return}
            blackView.alpha = 0
            guard let safeSettingsView = self.settingsView else{return}
            safeSettingsView.view.frame = CGRect(x: self.window.frame.origin.x, y: self.window.frame.maxY, width: self.window.frame.width, height: self.window.frame.height/2)
        }) { (completed: Bool) in
            if setting.name != "" && setting.name != "Cancel" {
                self.showControllerForSetting(setting: setting)
            }
        }
    }
}

extension AppCoordinator: SearchDisplayDelegate{
    func showSearch() {
        emptyView = UIView()
        guard let clearView = emptyView else {return}
        clearView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(searchDismissSelected)))
        clearView.backgroundColor = .clear
        clearView.frame = window.frame
        clearView.alpha = 0
        window.addSubview(clearView)
        searchBox = SearchBoxView(frame: CGRect(x: window.frame.origin.x-window.frame.width, y: window.frame.origin.y, width: window.frame.width-100, height: 100),dismissDelegate:self)
        guard let safeSearchBox = self.searchBox else{ return}
        window.addSubview(safeSearchBox.view)
        searchBox?.searchField.becomeFirstResponder()
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            clearView.alpha = 1
            safeSearchBox.view.frame = CGRect(x: self.window.frame.origin.x, y: self.window.frame.origin.x, width: self.window.frame.width-100, height: 90)
        }, completion: nil)
    }
    
    @objc private func searchDismissSelected(){
        UIView.animate(withDuration: 0.5) {
            guard let clearView = self.emptyView else {return}
            clearView.alpha = 0
            guard let safeSearchBox = self.searchBox else{ return}
            safeSearchBox.searchField.endEditing(true)
            safeSearchBox.view.frame = CGRect(x: self.window.frame.origin.x-self.window.frame.width, y: self.window.frame.origin.y, width: self.window.frame.width-100, height: 90)
        }
    }
    
    func showControllerForTerm(searchTerm: String) {
        let searchVC = SearchViewController(searchTerm: searchTerm, dataSource: SearchViewControllerDataSource(dependencies: dependencies))
        navController.pushViewController(searchVC, animated: true)
    }
}

extension AppCoordinator: SearchDismissDelegate{
    func handleDismiss(searchTerm: String) {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            guard let clearView = self.emptyView else {return}
            clearView.alpha = 0
            guard let safeSearchBox = self.searchBox else{ return}
            safeSearchBox.view.frame = CGRect(x: self.window.frame.origin.x-self.window.frame.width, y: self.window.frame.origin.y, width: self.window.frame.width-100, height: 90)
        }) { (completed: Bool) in
            if searchTerm != ""{
                self.showControllerForTerm(searchTerm: searchTerm)
            }
        }
    }
}
