//
//  VideoViewController.swift
//  YoutubeClone
//
//  Created by Luka on 20/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit
import WebKit

class VideoViewController: UIViewController{
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    let videoTitle: String
    let videoURL: URLRequest
    
    init(videoURL: URLRequest, title: String){
        self.videoTitle = title
        self.videoURL = videoURL
        super.init(nibName:nil, bundle:nil)
    }
    
    required init?(coder aDecoder: NSCoder) {return nil}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLbl.text = videoTitle
        webView.load(videoURL)
        webView.navigationDelegate = self
        activityIndicator.startAnimating()
    }
}

extension VideoViewController: WKNavigationDelegate{
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        UIView.animate(withDuration: 0.5) {
            self.activityIndicator.stopAnimating()
            self.activityIndicator.alpha = 0
            self.loadingView.alpha = 0
        }
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        UIView.animate(withDuration: 0.5) {
            self.activityIndicator.stopAnimating()
            self.activityIndicator.alpha = 0
            self.loadingView.alpha = 0
        }
    }
}

