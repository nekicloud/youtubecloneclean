//
//  SearchCell.swift
//  YoutubeClone
//
//  Created by Luka on 18/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit

class SearchCell: UITableViewCell{
    @IBOutlet weak var thumbnailImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var channelLabel: UILabel!
    @IBOutlet weak var viewLabel: UILabel!
    
    func setUpViews(video: Video){
        titleLabel.text = video.title
        channelLabel.text = video.channelName
        viewLabel.text = "\(video.numberOfViews) + DATE HERE"
    }
}
