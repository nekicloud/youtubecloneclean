//
//  SearchViewControllerDataSource.swift
//  YoutubeClone
//
//  Created by Luka on 18/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit

protocol SearchViewControllerDataSourceDependencies {
    var searchedVideosUseCase: SearchUseCase{get}
    var imageUseCase: ImageUseCase{get}
}

protocol SearchViewControllerDataSourceUpdateFeedback: class {
    func dataSourceUpdated()
}

class SearchViewControllerDataSource: NSObject{
    let cellId = "searchCell"
    var dependencies: SearchViewControllerDataSourceDependencies
    var videos = Videos()
    weak var feedback: SearchViewControllerDataSourceUpdateFeedback?
    
    init(dependencies: SearchViewControllerDataSourceDependencies){
        self.dependencies = dependencies
    }
    
    func fetchResults(for searchTerm: String){
        dependencies.searchedVideosUseCase.fetchSearchedVideos(searchTerm: searchTerm) {[unowned self] response in
            switch response{
            case .success(let videos):
                self.videos = videos
                self.feedback?.dataSourceUpdated()
            case .error(let error):
                print(error)
            }
        }
    }
    
    private func fetchImage(for path: String, for cell: SearchCell){
        dependencies.imageUseCase.fetchImage(path: path){ response in
            switch response{
            case .success(let image):
                let image = UIImage(data: image.image)
                cell.thumbnailImage.image = image
            case .error(let error):
                print(error)
            }
        }
    }
}

extension SearchViewControllerDataSource: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return videos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        guard let searchCell = cell as? SearchCell else { return cell}
        let video = videos[indexPath.row]
        searchCell.setUpViews(video: video)
        fetchImage(for: video.thumbnailImagePath, for: searchCell)
        return searchCell
    }
}
