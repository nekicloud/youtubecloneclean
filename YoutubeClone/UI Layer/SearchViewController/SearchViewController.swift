//
//  SearchViewController.swift
//  YoutubeClone
//
//  Created by Luka on 18/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController{
    @IBOutlet weak var tableView: UITableView!
    let searchTerm: String
    let dataSource: SearchViewControllerDataSource
    
    init(searchTerm: String, dataSource: SearchViewControllerDataSource){
        self.searchTerm = searchTerm
        self.dataSource = dataSource
        super.init(nibName:nil, bundle:nil)
    }
    
    required init?(coder aDecoder: NSCoder) {return nil}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpTableView()
        navigationController?.navigationBar.tintColor = .white
        self.navigationItem.title = " "
        dataSource.fetchResults(for: searchTerm)
    }
    
    private func setUpTableView() {
        tableView.register(UINib(nibName: "SearchCell", bundle: nil), forCellReuseIdentifier: "searchCell")
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = dataSource
        dataSource.feedback = self
    }
}

extension SearchViewController: SearchViewControllerDataSourceUpdateFeedback{
    func dataSourceUpdated() {
        tableView.reloadData()
    }
}
extension SearchViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        guard let searchCell = cell as? SearchCell else {return}
        print(searchCell)
    }
}
