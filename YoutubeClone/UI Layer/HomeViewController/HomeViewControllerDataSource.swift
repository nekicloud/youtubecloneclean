//
//  HomeViewControllerDataSource.swift
//  YoutubeClone
//
//  Created by Luka on 17/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit

protocol HomeViewControllerDataSourceDependencies{
    var homeFeedUseCase: HomeVideosUseCase{get}
    var trendingFeedUseCase: TrendingVideosUseCase{get}
    var subscriptionFeedUseCase: SubscriptionVideosUseCase{get}
    var imageUseCase: ImageUseCase{get}
    var channelUseCase: ChannelUseCase{get}
}

protocol DataSourceUpdateFeedback {
    func dataSourceUpdated()
}

class HomeViewControllerDataSource: NSObject{
    let cellId = "videoFeedCell"
    let accountCellId = "accountCell"
    var dependencies: HomeViewControllerDataSourceDependencies
    var videoFeeds: [String:Videos] = [:]
    var images: [String:UIImage] = [:]
    var feedback: DataSourceUpdateFeedback?
    var cellLauncherDelegate: VideoViewControllerDelegate?
    
    init(dependencies: HomeViewControllerDataSourceDependencies){
        self.dependencies = dependencies
        super.init()
    }
    
    func fetchHome(){
        dependencies.homeFeedUseCase.fetchHomeVideos(){ [unowned self] response in
            switch response{
            case .success(let videos):
                for object in videos{
                    self.fetchChannelURL(for: object)
                    self.fetchThumbnailImage(for: object.thumbnailImagePath)
                }
                self.videoFeeds["home"] = videos
                self.feedback?.dataSourceUpdated()
            case .error(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    func fetchTrending(){
        dependencies.trendingFeedUseCase.fetchTrendingVideos(){ [unowned self] response in
            switch response{
            case .success(let videos):
                for object in videos{
                    self.fetchChannelURL(for: object)
                    self.fetchThumbnailImage(for: object.thumbnailImagePath)
                }
                self.videoFeeds["trending"] = videos
                self.feedback?.dataSourceUpdated()
            case .error(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    func fetchSubscription(){
        dependencies.subscriptionFeedUseCase.fetchSubscriptionVideos(){ [unowned self] response in
            switch response{
            case .success(let videos):
                for object in videos{
                    self.fetchChannelURL(for: object)
                    self.fetchThumbnailImage(for: object.thumbnailImagePath)
                }
                self.videoFeeds["subscription"] = videos
                self.feedback?.dataSourceUpdated()
            case .error(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    private func fetchChannelURL(for video: Video){
        dependencies.channelUseCase.fetchChannelImageURL(for: video.channelId){ response in
            switch response{
            case .success(let channelURL):
                self.fetchChannelImage(for: channelURL, for: video.channelId)
            case .error(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    private func fetchThumbnailImage(for path: String){
        dependencies.imageUseCase.fetchImage(path: path){ response in
            switch response{
            case .success(let result):
                let image = UIImage(data: result.image)
                self.images[path] = image
                self.feedback?.dataSourceUpdated()
            case .error(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    private func fetchChannelImage(for path: String, for id: String){
        dependencies.imageUseCase.fetchImage(path: path){ response in
            switch response{
            case .success(let result):
                let image = UIImage(data: result.image)
                self.images[id] = image
                self.feedback?.dataSourceUpdated()
            case .error(let error):
                print(error.localizedDescription)
            }
        }
    }
}

extension HomeViewControllerDataSource: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 3{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: accountCellId, for: indexPath)
            guard let accountCell = cell as? AccountCell else {return cell}
            return accountCell
        }

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath)
        guard let feedCell = cell as? SlidingVideoFeedCell else {return cell}
        feedCell.launcherDelegate = cellLauncherDelegate
        switch indexPath.row {
        case 0:
            feedCell.setUpViews(videos: videoFeeds["home"], images: images)
        case 1:
            feedCell.setUpViews(videos: videoFeeds["trending"], images: images)
        case 2:
            feedCell.setUpViews(videos: videoFeeds["subscription"], images: images)
        default:
            print("out of range")
        }
            return feedCell
        }
}

extension HomeViewControllerDataSource: ImageDelegate{
    func fetchImage(for video: Video, for cell: VideoCell) {
        print("")
    }
}

