//
//  SettingCell.swift
//  YoutubeClone
//
//  Created by Luka on 16/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit

class SettingCell: UICollectionViewCell{
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var settingImage: UIImageView! {
        didSet{
            settingImage.tintColor = .darkGray
        }
    }
    
    override var isHighlighted: Bool {
        didSet{
            backgroundColor = isHighlighted ? .darkGray : .white
            nameLbl.textColor = isHighlighted ? .white : .black
            settingImage.tintColor = isHighlighted ? .white : .darkGray
        }
    }
    
    func updateViews(imageName: String, name: String){
        self.nameLbl.text = name
        self.settingImage.image = UIImage(named: imageName)
        self.backgroundColor = .white
    }
}

struct Setting{
    let name: String
    let imageName: String
    
    init(name: String, imageName: String) {
        self.name = name
        self.imageName = imageName
    }
}
