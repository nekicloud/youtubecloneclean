//
//  SettingsView.swift
//  YoutubeClone
//
//  Created by Luka on 16/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit

protocol SettingsDismissDelegate{
    func handleDismiss(setting: Setting)
}

class SettingsView:UIViewController{
    @IBOutlet weak var collectionView:UICollectionView!
    let cellId = "cellId"
    let dismissDelegate: SettingsDismissDelegate
    
    let settings: [Setting] = {
        return [Setting(name: "Settings", imageName: "settings"),
                Setting(name: "Terms & privacy policy", imageName: "privacy"),
                Setting(name: "Send Feedback", imageName: "feedback"),
                Setting(name: "Help", imageName: "help"),
                Setting(name: "Switch Account", imageName: "switch_account"),
                Setting(name: "Cancel", imageName: "cancel")]
    }()
    
    init(frame: CGRect, dismissDelegate: SettingsDismissDelegate) {
        self.dismissDelegate = dismissDelegate
        super.init(nibName: nil, bundle: nil)
        view.frame = frame
        setUpSettings()
    }
    
    required init?(coder aDecoder: NSCoder) {return nil}
    
    private func setUpSettings(){
        collectionView.register(UINib(nibName: "SettingCell", bundle: nil), forCellWithReuseIdentifier: cellId)
        collectionView.delegate = self
        collectionView.dataSource = self
    }
}

extension SettingsView:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return settings.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath)
        guard let settingCell = cell as? SettingCell else {return cell}
        settingCell.updateViews(imageName: settings[indexPath.row].imageName, name: settings[indexPath.row].name)
        return settingCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let setting = settings[indexPath.item]
        dismissDelegate.handleDismiss(setting: setting)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
extension SettingsView:UICollectionViewDelegate{}


extension SettingsView:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellHeight: CGFloat = view.frame.height/CGFloat(settings.count)
        return CGSize(width:view.frame.width, height: cellHeight)
    }
}
