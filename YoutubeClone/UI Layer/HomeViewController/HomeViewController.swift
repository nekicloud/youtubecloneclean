//
//  ViewController.swift
//  YoutubeClone
//
//  Created by Luka on 15/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit

protocol SettingsDisplayDelegate{
    func showSettings()
    func showControllerForSetting(setting: Setting)
}

protocol SearchDisplayDelegate{
    func showSearch()
    func showControllerForTerm(searchTerm: String)
}

class HomeViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    let menuController = MenuController()
    var dataSource: HomeViewControllerDataSource
    var videoDelegate: VideoViewControllerDelegate?
    var settingsDelegate: SettingsDisplayDelegate?
    var searchDelegate: SearchDisplayDelegate?
    
    init(dependencies: HomeViewControllerDataSourceDependencies) {
        self.dataSource = HomeViewControllerDataSource(dependencies: dependencies)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {return nil}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationBar()
        setUpNavigationBarButtons()
        setUpCollectionView()
        setUpMenuBar()
        dataSource.fetchHome()
        dataSource.fetchTrending()
        dataSource.fetchSubscription()
    }
    
    private func setUpNavigationBar(){
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 32, height: view.frame.height))
        titleLabel.text = "Home"
        titleLabel.textColor = .white
        titleLabel.font = UIFont.systemFont(ofSize: 20)
        navigationItem.titleView = titleLabel
    }
    
    private func setUpNavigationBarButtons() {
        let searchBarButton = UIBarButtonItem(image: UIImage(named: "search_icon"), style: .plain, target: self, action: #selector(handleSearch))
        let moreBarButton = UIBarButtonItem(image: UIImage(named: "nav_more_icon"), style: .plain, target: self, action: #selector(handleMore))
        navigationItem.rightBarButtonItems = [moreBarButton, searchBarButton]
    }
    
    private func setUpMenuBar(){
        menuController.buttonDelegate = self
        guard let menuBar = menuController.view else {return}
        view.addSubview(menuBar)
        menuBar.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            menuBar.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
            menuBar.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor),
            menuBar.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            menuBar.heightAnchor.constraint(equalToConstant: 50)
        ])
    }
 
    @objc private func handleMore(){
        settingsDelegate?.showSettings()
    }

    @objc private func handleSearch(){
        searchDelegate?.showSearch()
    }
    
    private func setUpCollectionView(){
        dataSource.cellLauncherDelegate = videoDelegate
        collectionView.dataSource = dataSource
        dataSource.feedback = self
        collectionView.delegate = self
        collectionView.contentInsetAdjustmentBehavior = .never
        collectionView.register(UINib(nibName: "SlidingVideoFeedCell", bundle: nil), forCellWithReuseIdentifier: "videoFeedCell")
        collectionView.register(UINib(nibName: "AccountCell", bundle: nil), forCellWithReuseIdentifier: "accountCell")
    }
}

extension HomeViewController: DataSourceUpdateFeedback{
    func dataSourceUpdated() {
        collectionView.reloadData()
    }
}

extension HomeViewController: MenuButtonFeedback{
    func scrollToMenuIndex(menuIndex: Int){
        let index = IndexPath(item: menuIndex, section: 0)
        collectionView.scrollToItem(at: index, at: .init(rawValue: 0), animated: true)
    }
}

extension HomeViewController:UICollectionViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        menuController.horizontalBarLeftAnchor?.constant = scrollView.contentOffset.x/4
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let index = targetContentOffset.pointee.x/view.frame.width
        let indexPath = IndexPath(item: Int(index), section: 0)
        menuController.menuCollectionView.selectItem(at: indexPath, animated: true, scrollPosition: .init(rawValue: 0))
    }
}

extension HomeViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:view.frame.width, height: collectionView.frame.height)
    }
}

