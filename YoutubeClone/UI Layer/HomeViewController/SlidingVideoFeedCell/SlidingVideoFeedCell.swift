//
//  FeedCell.swift
//  YoutubeClone
//
//  Created by Luka on 16/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit
protocol VideoViewControllerDelegate{
    func showVideoFor(url: URLRequest, title: String)
}

class SlidingVideoFeedCell: UICollectionViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    var dataSource: SlidingVideoFeedCellDataSource?
    var launcherDelegate: VideoViewControllerDelegate?
    
    func setUpViews(videos: Videos?,images: [String:UIImage]){
        guard let videoFeed = videos else {return}
        self.dataSource = SlidingVideoFeedCellDataSource(videos: videoFeed,images: images)
        collectionView.dataSource = dataSource
        collectionView.delegate = self
        collectionView.register(UINib(nibName: "VideoCell", bundle: nil), forCellWithReuseIdentifier: "videoCell")
    }
}

extension SlidingVideoFeedCell: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let currentCell = collectionView.cellForItem(at: indexPath)
        guard let videoCell = currentCell as? VideoCell else{return}
        guard let id = videoCell.videoID else {return}
        guard let url = URL(string: "https://www.youtube.com/embed/\(id)") else {return}
        launcherDelegate?.showVideoFor(url: URLRequest(url: url), title: videoCell.titleLbl.text ?? "notitle")
    }
}

extension SlidingVideoFeedCell:UICollectionViewDelegateFlowLayout{
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            let heightOfCellTo16by9ratio = (frame.width - 16 - 16) * 9 / 16
            return CGSize(width:frame.width, height: heightOfCellTo16by9ratio+78)
        }
}
