//
//  AccountCell.swift
//  YoutubeClone
//
//  Created by Luka on 19/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit

class AccountCell: UICollectionViewCell{
    let dataSource = AccountCellDataSource()
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.dataSource = dataSource
            tableView.delegate = self
            tableView.separatorStyle = .none
            tableView.register(UINib(nibName: "AccountCellTableViewCell", bundle: nil), forCellReuseIdentifier: "accountTableViewCell")
            tableView.rowHeight = 50
        }
    }
}

extension AccountCell: UITableViewDelegate{}
