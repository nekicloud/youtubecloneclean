//
//  AccountCellDataSource.swift
//  YoutubeClone
//
//  Created by Luka on 19/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit

class AccountCellDataSource: NSObject{
    let cellId = "accountTableViewCell"
}

extension AccountCellDataSource: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        guard let accountTableCell = cell as? AccountCellTableViewCell else { return cell}
        accountTableCell.placeholderLabel.text = "\(indexPath.row)" + "fafafafafaf"
        return accountTableCell
    }
}
