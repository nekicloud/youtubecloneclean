//
//  VideoCell.swift
//  YoutubeClone
//
//  Created by Luka on 15/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit

class VideoCell: UICollectionViewCell {
    @IBOutlet weak var thumbnailImage: UIImageView!
    @IBOutlet weak var profileImage: UIImageView! {
        didSet{
            profileImage.layer.cornerRadius = (profileImage.frame.width)/2
        }
    }
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var subtitleLbl: UILabel!
    var videoID: String?
    
    func setUpViews(video: Video){
        self.titleLbl.text = video.title
        let views = "\(video.numberOfViews)"
        self.subtitleLbl.text = video.channelName + " " + views + " DATE HERE"
        self.videoID = video.videoID
    }
}
