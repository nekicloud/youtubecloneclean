//
//  SlidingVideoFeedCellDataSource.swift
//  YoutubeClone
//
//  Created by Luka on 17/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit

protocol ImageDelegate{
    func fetchImage(for video: Video, for cell: VideoCell)
}

class SlidingVideoFeedCellDataSource: NSObject{
    let videoCellId = "videoCell"
    var videos: Videos
    var imageDelegate: ImageDelegate?
    var images: [String : UIImage] = [:]
    
    init(videos: Videos, images: [String:UIImage]){
        self.videos = videos
        self.images = images
    }
}

extension SlidingVideoFeedCellDataSource: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return videos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: videoCellId, for: indexPath)
        guard let videoCell = cell as? VideoCell else { return cell}
        videoCell.setUpViews(video: videos[indexPath.row])
        videoCell.profileImage.image = images[videos[indexPath.row].channelId]
        videoCell.thumbnailImage.image = images[videos[indexPath.row].thumbnailImagePath]
        return videoCell
    }
}


