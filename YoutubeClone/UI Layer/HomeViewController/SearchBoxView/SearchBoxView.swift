//
//  SearchView.swift
//  YoutubeClone
//
//  Created by Luka on 18/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit

protocol SearchDismissDelegate{
    func handleDismiss(searchTerm: String)
}

class SearchBoxView:UIViewController{
    @IBOutlet weak var searchField: UITextField!
    let dismissDelegate: SearchDismissDelegate
    
    
    init(frame: CGRect, dismissDelegate: SearchDismissDelegate) {
        self.dismissDelegate = dismissDelegate
        super.init(nibName: nil, bundle: nil)
        view.frame = frame
    }
    
    required init?(coder aDecoder: NSCoder) {return nil}
    
    override func viewDidLoad() {
        searchField.delegate = self
    }
}

extension SearchBoxView:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        dismissDelegate.handleDismiss(searchTerm: searchField.text ?? "")
        return true
    }
}
