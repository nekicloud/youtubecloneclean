//
//  MenuCell.swift
//  YoutubeClone
//
//  Created by Luka on 15/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit

class MenuCell: UICollectionViewCell{
    @IBOutlet weak var itemImage: UIImageView! {
        didSet{
            itemImage.tintColor = UIColor(red: 91/255, green: 14/255, blue: 13/255, alpha: 1)
        }
    }
    override var isHighlighted: Bool {
        didSet{
            itemImage.tintColor = isHighlighted ? .white : UIColor(red: 91/255, green: 14/255, blue: 13/255, alpha: 1)
        }
    }
    override var isSelected: Bool{
        didSet{
            itemImage.tintColor = isSelected ? .white : UIColor(red: 91/255, green: 14/255, blue: 13/255, alpha: 1)
        }
    }
}
