//
//  MenuCoordinator.swift
//  YoutubeClone
//
//  Created by Luka on 15/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit

class MenuController: UIViewController {
    @IBOutlet weak var menuCollectionView:UICollectionView!
    var horizontalBarLeftAnchor: NSLayoutConstraint?
    let dataSource:MenuControllerDataSource
    var buttonDelegate: MenuButtonFeedback?
    
    init() {
        self.dataSource = MenuControllerDataSource()
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {return nil}

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpHorizontalBar()
        setUpMenuCollectionView()
    }
    
    private func setUpHorizontalBar(){
        let horizontalBarView = UIView()
        horizontalBarView.backgroundColor = .white
        horizontalBarView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(horizontalBarView)
        horizontalBarLeftAnchor = horizontalBarView.leftAnchor.constraint(equalTo: self.view.leftAnchor)
        horizontalBarLeftAnchor?.isActive = true
        horizontalBarView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        horizontalBarView.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 1/4).isActive = true
        horizontalBarView.heightAnchor.constraint(equalToConstant: 4).isActive = true
    }
    
    private func setUpMenuCollectionView(){
        menuCollectionView.dataSource = dataSource
        menuCollectionView.delegate = self
        menuCollectionView.register(UINib(nibName: "MenuCell", bundle: nil), forCellWithReuseIdentifier: "menuCell")
        menuCollectionView.backgroundColor = UIColor(red: 230/255, green: 32/255, blue: 31/255, alpha: 1)
        menuCollectionView.selectItem(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: .init(rawValue: 0))
    }
}

extension MenuController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        buttonDelegate?.scrollToMenuIndex(menuIndex: indexPath.item)
    }
}

extension MenuController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:view.frame.width/4, height: 50)
    }
}
