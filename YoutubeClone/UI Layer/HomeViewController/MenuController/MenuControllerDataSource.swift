//
//  MenuControllerDataSource.swift
//  YoutubeClone
//
//  Created by Luka on 17/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit

protocol MenuButtonFeedback{
    func scrollToMenuIndex(menuIndex: Int)
}

class MenuControllerDataSource: NSObject{
    let cellId = "menuCell"
    let menuIconNames = ["home","trending","subscriptions","account"]
}

extension MenuControllerDataSource:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menuIconNames.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath)
        guard let menuCell = cell as? MenuCell else {return cell}
        menuCell.itemImage.image = UIImage(named: menuIconNames[indexPath.row])
        return menuCell
    }
}
