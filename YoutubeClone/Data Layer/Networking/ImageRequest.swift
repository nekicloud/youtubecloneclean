//
//  ChannelRequest.swift
//  YoutubeClone
//
//  Created by Luka on 16/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

class ImageRequest{
    let imagePath: String
    
    init(path: String) {
        imagePath = path
    }
    
    var urlRequest: URLRequest? {
        let urlString = imagePath
        guard let url = URL(string: urlString) else {return nil}
        return URLRequest(url: url)
    }
}

