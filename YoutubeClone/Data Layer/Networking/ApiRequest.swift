//
//  ApiRequest.swift
//  YoutubeClone
//
//  Created by Luka on 16/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

struct ApiRequest{
    let baseUrlString = "https://www.googleapis.com/youtube/v3/"
//    let apiKey = "&key=AIzaSyCs0NVzHNXRnRcUubKbwbXOet-k0xUUcGg" backup api key
    let apiKey = "&key=AIzaSyCG2feZn5HeyrBEJ2LEqRR2y7KWENeXeDM"
    let endpoint: Endpoint
    
    init(endpoint: Endpoint) {
        self.endpoint = endpoint
    }
    
    var urlRequest: URLRequest? {
        let urlString = baseUrlString + endpoint.parameters + apiKey
        guard let url = URL(string: urlString) else { return nil}
        return URLRequest(url: url)
    }
}

enum RequestError: Error {
    case urlRequestFailed
}
