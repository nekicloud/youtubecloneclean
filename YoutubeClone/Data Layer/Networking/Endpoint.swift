//
//  Endpoint.swift
//  YoutubeClone
//
//  Created by Luka on 16/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

enum Endpoint {
    case search(term: String)
    case channel(channelId: String)
    case trending(region: String)
    
    var parameters: String{
        switch self{
        case let .search(term: term):
            let spacedTerm = term.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            return "search?part=snippet&type=video&q='\(spacedTerm ?? term)'&maxResults=30"
        case let .channel(channelId: ID):
            return "channels?part=snippet&id=\(ID)"
        case let .trending(region: region):
            return "videos?part=snippet,statistics&chart=mostpopular&regionCode=\(region)"
        }
    }
}
