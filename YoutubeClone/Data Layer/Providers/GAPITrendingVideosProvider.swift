//
//  GAPITrendingVideosProvider.swift
//  YoutubeClone
//
//  Created by Luka on 19/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

class GAPITrendingVideosProvider: TrendingVideosProvider{
    let webService: WebService
    
    init(webService: WebService = WebServiceProvider(session: DataNetworksession())){
        self.webService = webService
    }
    
    func fetchTrendingVideos(completion: @escaping (Response<Videos>) -> Void) {
        guard let request = ApiRequest(endpoint: .trending(region: "GB")).urlRequest else {return}
        webService.execute(request){ (response: Response<ResponseData>) in
            switch response{
            case .success(let result):
                completion(.success(self.createTrendingVideos(from: result)))
            case .error(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    private func createTrendingVideos(from result: ResponseData) -> Videos {
        var videos = Videos()
        for object in result.items{
            let nextVideo = GAPIVideo(
                title: object.snippet.title,
                numberOfViews: object.statistics.viewCount.count,
                uploadDate: object.snippet.publishedAt,
                thumbnailImagePath: object.snippet.thumbnails.high.url,
                channelName: object.snippet.channelTitle,
                channelImagePath: "",
                channelId: object.snippet.channelID,
                videoID: object.id)
            videos.append(nextVideo)
        }
        return videos
    }
}
