//
//  GAPISearchedVideosProvider.swift
//  YoutubeClone
//
//  Created by Luka on 19/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

class GAPISearchProvider: SearchProvider{
    let webService: WebService
    
    init(webService: WebService = WebServiceProvider(session: DataNetworksession())){
        self.webService = webService
    }
    
    func fetchSearchedVideos(searchTerm: String, completion: @escaping (Response<Videos>) -> Void) {
        guard let request = ApiRequest(endpoint: .search(term: searchTerm)).urlRequest else {return}
        webService.execute(request){ (response: Response<SearchResponseData>) in
            switch response{
            case .success(let result):
                completion(.success(self.createSearchVideos(from: result)))
            case .error(let error):
                print(error)
            }
        }
    }
    
    private func createSearchVideos(from result: SearchResponseData) -> Videos {
        var videos = Videos()
        for object in result.items{
            let nextVideo = GAPIVideo(
                title: object.snippet.title,
                numberOfViews: 0,
                uploadDate: object.snippet.publishedAt,
                thumbnailImagePath: object.snippet.thumbnails.high.url,
                channelName: object.snippet.channelTitle,
                channelImagePath: "",
                channelId: object.snippet.channelID,
                videoID: object.id.videoID)
            videos.append(nextVideo)
        }
        return videos
    }
}


struct SearchResponseData: Codable {
    let items: [SearchItem]
}

struct SearchItem: Codable {
    let snippet: SearchSnippet
    let id: ID
}

struct ID: Codable {
    let videoID: String
    
    enum CodingKeys: String, CodingKey {
        case videoID = "videoId"
    }
}

struct SearchSnippet: Codable {
    let publishedAt, channelID, title: String
    let thumbnails: Thumbnails
    let channelTitle: String
    
    enum CodingKeys: String, CodingKey {
        case publishedAt
        case channelID = "channelId"
        case title
        case thumbnails, channelTitle
    }
}
