//
//  GAPIChannelProvider.swift
//  YoutubeClone
//
//  Created by Luka on 19/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

class GAPIChannelProvider: ChannelProvider{
    let webService: WebService
    
    init(webService: WebService = WebServiceProvider(session: DataNetworksession())){
        self.webService = webService
    }
    
    func fetchChannelImageURL(for Id: String, completion: @escaping (Response<String>) -> Void) {
        guard let request = ApiRequest(endpoint: .channel(channelId: Id)).urlRequest else{return}
        webService.execute(request){ (response: Response<ChannelData>) in
            switch response{
            case .success(let result):
                guard let imagePath = result.items.first?.snippet.thumbnails.high.url else {return}
                completion(.success(imagePath))
            case .error(let error):
                print(request)
                print(error)
            }
        }
    }
}

struct ChannelData: Codable{
    let items: [ChannelItem]
}

struct ChannelItem: Codable {
    let snippet: ChannelSnippet
}

struct ChannelSnippet: Codable {
    let thumbnails: ChannelThumbnails
    
    enum CodingKeys: String, CodingKey {
        case thumbnails
    }
}

struct ChannelThumbnails: Codable {
    let thumbnailsDefault, medium, high: Default
    
    enum CodingKeys: String, CodingKey {
        case thumbnailsDefault = "default"
        case medium, high
    }
}

