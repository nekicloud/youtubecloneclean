//
//  GAPIVideoProvider.swift
//  YoutubeClone
//
//  Created by Luka on 17/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//
import Foundation

class GAPIHomeVideosProvider: HomeVideosProvider{
    let webService: WebService
    
    init(webService: WebService = WebServiceProvider(session: DataNetworksession())){
        self.webService = webService
    }
    
    func fetchHomeVideos(completion: @escaping (Response<Videos>) -> Void) {

    }
    
    private func createHomeVideos(from result: ResponseData) -> Videos {
        var videos = Videos()
        for object in result.items{
            let nextVideo = GAPIVideo(
                title: object.snippet.title,
                numberOfViews: object.statistics.viewCount.count,
                uploadDate: object.snippet.publishedAt,
                thumbnailImagePath: object.snippet.thumbnails.high.url,
                channelName: object.snippet.channelTitle,
                channelImagePath: "",
                channelId: object.snippet.channelID,
                videoID: object.id
                )
            videos.append(nextVideo)
        }
        return videos
    }
}

struct GAPIVideo: Video{
    var title: String
    var numberOfViews: Int
    var uploadDate: Date
    var datetxt: String
    var thumbnailImagePath: String
    var channelName: String
    var channelImagePath: String
    var channelId: String
    var videoID: String
    
    init(title: String, numberOfViews: Int, uploadDate: String,thumbnailImagePath: String, channelName: String, channelImagePath: String, channelId: String, videoID: String) {
        self.title = title
        self.numberOfViews = numberOfViews
        self.datetxt = uploadDate
        self.thumbnailImagePath = thumbnailImagePath
        self.channelName = channelName
        self.channelImagePath = channelImagePath
        self.channelId = channelId
        self.uploadDate = Date()
        self.videoID = videoID
    }
}


struct ResponseData: Codable {
    let items: [Item]
}

struct Item: Codable {
    let snippet: Snippet
    let statistics: Statistics
    let id: String
}

struct Snippet: Codable {
    let publishedAt, channelID, title: String
    let thumbnails: Thumbnails
    let channelTitle: String
    
    enum CodingKeys: String, CodingKey {
        case publishedAt
        case channelID = "channelId"
        case title
        case thumbnails, channelTitle
    }
}


struct Thumbnails: Codable {
    let high: Default
    
    enum CodingKeys: String, CodingKey {
        case high
    }
}


struct Default: Codable {
    let url: String
}


struct Statistics: Codable {
    let viewCount: String
}
