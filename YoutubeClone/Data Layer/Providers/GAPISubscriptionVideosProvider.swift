//
//  GAPISubscriptionVideosProvider.swift
//  YoutubeClone
//
//  Created by Luka on 19/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

class GAPISubscriptionVideosProvider: SubscriptionVideosProvider{
    let webService: WebService
    
    init(webService: WebService = WebServiceProvider(session: DataNetworksession())){
        self.webService = webService
    }
    
    func fetchSubscriptionVideos(completion: @escaping (Response<Videos>) -> Void) {
    }
    
    private func createSubscriptionVideos(from result: ResponseData) -> Videos {
        var videos = Videos()
        for object in result.items{
            let nextVideo = GAPIVideo(
                title: object.snippet.title,
                numberOfViews: object.statistics.viewCount.count,
                uploadDate: object.snippet.publishedAt,
                thumbnailImagePath: object.snippet.thumbnails.high.url,
                channelName: object.snippet.channelTitle,
                channelImagePath: "",
                channelId: object.snippet.channelID,
                videoID: object.id)
            videos.append(nextVideo)
        }
        return videos
    }
}
