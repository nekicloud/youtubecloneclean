//
//  GAPIImageProvider.swift
//  YoutubeClone
//
//  Created by Luka on 17/06/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import Foundation

class GAPIImageProvider: ImageProvider{
    let webService: WebService
    var imageCache: [String : Data] = [:]
    
    init(webService: WebService = WebServiceProvider(session: DataNetworksession())){
        self.webService = webService
    }
    
    func fetchImage(for path: String, completion: @escaping (Response<Data>) -> Void) {
        if let imageData = imageCache[path]{
            completion(.success(imageData))
        }
        guard let imageRequest = ImageRequest(path: path).urlRequest else {return}
        
        webService.execute(imageRequest) {[weak self] (response: Response<Data>) in
            switch response{
            case .success(let imageData):
                self?.imageCache[path] = imageData
                completion(.success(imageData))
            case .error(let error):
                print(error)
            }
        }
    }
}
